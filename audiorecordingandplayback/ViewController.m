//
//  ViewController.m
//  audiorecordingandplayback
//
//  Created by James Cash on 24-03-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;

@interface ViewController ()

@property (strong,nonatomic) NSURL *savedFileURL;
@property (strong,nonatomic) AVAudioRecorder *recorder;
@property (strong,nonatomic) AVAudioPlayer *player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSURL *documentDir = [[[NSFileManager defaultManager]
                          URLsForDirectory:NSDocumentDirectory
                          inDomains:NSUserDomainMask]
                          firstObject];
    NSLog(@"URL for our documents dir = %@", documentDir);

    self.savedFileURL = [documentDir URLByAppendingPathComponent:@"recording.m4a"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRecording:(UIButton *)sender {
    if (self.recorder.isRecording) {
        // stop recording
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        [self.recorder stop];
    } else {
        // start recording
        [sender setTitle:@"Stop" forState:UIControlStateNormal];
        NSError *err = nil;
        self.recorder = [[AVAudioRecorder alloc]
                         initWithURL:self.savedFileURL
                         settings:@{AVSampleRateKey: @(44100),
                                    AVFormatIDKey: @(kAudioFormatMPEG4AAC),
                                    AVNumberOfChannelsKey: @(2)}
                         error:&err];
        if (err) {
            NSLog(@"Error creating recorder %@", err.localizedDescription);
        }
        [self.recorder record];
    }
}

- (IBAction)togglePlaying:(UIButton *)sender {
    if (self.player.isPlaying) {
        // stop player
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        [self.player stop];
    } else {
        // start player
        [sender setTitle:@"Stop" forState:UIControlStateNormal];
        NSError *err = nil;
        self.player = [[AVAudioPlayer alloc]
                       initWithContentsOfURL:self.savedFileURL
                       error:&err];
        if (err) {
            NSLog(@"Error creating player %@", err.localizedDescription);
        }
        [self.player play];
    }
}

@end
